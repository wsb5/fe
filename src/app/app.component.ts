import { Component, OnInit } from '@angular/core';
import { AuthService, JWTService, ProgressBarService } from './core/services';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  public isCollapsed = false;
  public showProgressBar$: Observable<boolean>;
  public readonly menuItems = [
    {
      name: 'Users',
      icon: 'user',
      routerLink: 'users',
      isMobile: false,
      exact: true,
    },
    {
      name: 'Categories',
      icon: 'folder',
      routerLink: 'categories',
      isMobile: false,
      exact: true,
    },
    {
      name: 'Sign out',
      icon: 'logout',
      routerLink: '/auth/log-out',
      isMobile: false,
      exact: true,
    }
  ];

  constructor(
    private readonly _authService: AuthService,
    private readonly _jwtService: JWTService,
    private readonly _router: Router,
    private readonly _progressBarService: ProgressBarService,
  ) {}

  public ngOnInit(): void {
    if (this._jwtService.identity) {
      this._authService.getStatus().subscribe(
        () => {},
        () => {
          this._jwtService.destroyIdentity();
          this._router.navigateByUrl('/auth/login');
        }
      );
    }

    this.showProgressBar$ = this._progressBarService.progressBar$;
  }
}
