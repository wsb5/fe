import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(
    private readonly _httpClient: HttpClient
  ) {}

  public get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient.get(`${ environment.apiUrl }${ path }`, { params });
  }

  public post(path: string, body = {}, params: HttpParams = new HttpParams()): Observable<any> {
    return this._httpClient.post(
      `${ environment.apiUrl }${ path }`,
      JSON.stringify(body),
      {
        params,
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

  public put(path: string, body = {}): Observable<any> {
    return this._httpClient.put(
      `${ environment.apiUrl }${ path }`,
      JSON.stringify(body),
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

  public delete(path: string): Observable<any> {
    return this._httpClient.delete(`${ environment.apiUrl }${ path }`);
  }
}
