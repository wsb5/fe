import { Injectable } from '@angular/core';
import { BaseErrorResolverService } from '../BaseErrorResolverService';

@Injectable({
  providedIn: 'root'
})
export class AuthErrorResolverService extends BaseErrorResolverService {
  constructor() {
    super();
    this.codes = {
      ...this.codes,
      InvalidCredentials: 'Invalid credentials.'
    };
  }
}
