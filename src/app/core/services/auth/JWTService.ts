import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IIdentity } from '../../interfaces';

@Injectable({
  providedIn: 'root'
})
export class JWTService {

  public get isAuthenticated$(): Observable<boolean> {
    return this._isAuthenticated$.asObservable();
  }

  private readonly _isAuthenticated$ = new BehaviorSubject(!!this.identity);

  public get identity(): IIdentity {
    const identity = localStorage.getItem('identity');
    return identity ? JSON.parse(identity) : null;
  }

  public set identity(identity: IIdentity) {
    localStorage.setItem('identity', JSON.stringify(identity));
    this._isAuthenticated$.next(true);
  }

  public get token(): string | null {
    return this.identity ? this.identity.token : null;
  }

  public get refreshToken(): string | null {
    return this.identity ? this.identity.refreshToken : null;
  }

  public destroyIdentity(): void {
    localStorage.removeItem('identity');
    this._isAuthenticated$.next(false);
  }
}
