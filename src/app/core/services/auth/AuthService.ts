import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IIdentity } from '../../interfaces';
import { ApiService } from '../ApiService';
import { JWTService } from './JWTService';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly _baseUrl = '/auth';

  constructor(
    private readonly _apiService: ApiService,
    private readonly _jwtService: JWTService,
  ) {}

  public getStatus(): Observable<void> {
    return this._apiService.get(`${ this._baseUrl }/status`);
  }

  public login(credentials: { username: string; password: string; }): Observable<IIdentity> {
    return this._apiService.post(`${ this._baseUrl }/login`, credentials);
  }

  public refreshToken(): Observable<{ token: string }> {
    return this._apiService.post(`${ this._baseUrl }/refresh-token`);
  }
}
