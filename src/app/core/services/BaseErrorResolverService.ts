export abstract class BaseErrorResolverService {

  protected codes: Record<string, string> = {
    'TokenExpired': 'Sign in again.'
  };
  private readonly _defaultMessage = 'Internal error.';

  public getMessage(code: string): string {
    return code in this.codes ? this.codes[code] : this._defaultMessage;
  }
}
