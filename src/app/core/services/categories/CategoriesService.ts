import { Injectable } from '@angular/core';
import { GenericCrudService } from '../GenericCrudService';
import { ApiService } from '../ApiService';
import { ICategory } from '../../interfaces';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends GenericCrudService<ICategory> {

  protected baseUrl = '/categories';

  protected constructor(
    protected readonly apiService: ApiService
  ) {
    super(apiService);
  }

  public getAllAsTree(search = ''): Observable<ICategory[]> {
    const params = new HttpParams({
      fromObject: {
        search
      }
    });
    return this.apiService.get(`${ this.baseUrl }/tree-list`, params);
  }
}
