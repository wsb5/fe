import { Injectable } from '@angular/core';
import { BaseErrorResolverService } from '../BaseErrorResolverService';

@Injectable({
  providedIn: 'root'
})
export class CategoriesErrorResolverService extends BaseErrorResolverService {
  constructor() {
    super();
    this.codes = {
      ...this.codes,
      'CategoryNotFound': 'Kategorie nicht gefunden.'
    };
  }
}
