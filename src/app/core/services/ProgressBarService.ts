import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {
  private readonly _progressBar$ = new BehaviorSubject<boolean>(false);

  public get progressBar$(): Observable<boolean> {
    return this._progressBar$.asObservable();
  }

  public set progressBar(val: boolean) {
    this._progressBar$.next(val);
  }
}
