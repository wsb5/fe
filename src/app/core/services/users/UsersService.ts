import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../ApiService';
import { IUser } from '../../interfaces';
import { GenericCrudService } from '../GenericCrudService';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends GenericCrudService<IUser> {

  protected baseUrl = '/users';

  protected constructor(
    protected readonly apiService: ApiService
  ) {
    super(apiService);
  }

  public create(obj: IUser): Observable<{ password: string; }> {
    return super.create(obj);
  }

  public resetPassword(): Observable<{ password: string }> {
    return this.apiService.post(`${ this.baseUrl }/reset-password`);
  }
}
