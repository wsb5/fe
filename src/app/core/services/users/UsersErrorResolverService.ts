import { Injectable } from '@angular/core';
import { BaseErrorResolverService } from '../BaseErrorResolverService';

@Injectable({
  providedIn: 'root'
})
export class UsersErrorResolverService extends BaseErrorResolverService {
  constructor() {
    super();
    this.codes = {
      ...this.codes,
      UserNotFound: 'Benutzer nicht gefunden.',
      UsernameInUse: 'Benutzername verwendet.',
      EmailInUse: 'E-Mail wird verwendet.',
    };
  }
}
