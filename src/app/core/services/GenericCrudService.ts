import { ApiService } from './ApiService';
import { Observable } from 'rxjs';
import { HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

export abstract class GenericCrudService<T> {

  protected baseUrl = '';

  protected constructor(
    protected readonly apiService: ApiService
  ) {}

  public getAll(search = ''): Observable<T[]> {
    const params = new HttpParams({
      fromObject: {
        search
      }
    });
    return this.apiService.get(`${ this.baseUrl }`, params);
  }

  public getById(id: number): Observable<T> {
    return this.apiService.get(`${ this.baseUrl }/${ id }`);
  }

  public create(obj: T): Observable<any> {
    return this.apiService.post(`${ this.baseUrl }`, obj);
  }

  public update(id: number, obj: T): Observable<any> {
    return this.apiService.put(`${ this.baseUrl }/${ id }`, obj);
  }

  public delete(id: number): Observable<any> {
    return this.apiService.delete(`${ this.baseUrl }/${ id }`);
  }
}
