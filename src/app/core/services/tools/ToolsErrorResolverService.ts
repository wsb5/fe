import { Injectable } from '@angular/core';
import { BaseErrorResolverService } from '../BaseErrorResolverService';

@Injectable({
  providedIn: 'root'
})
export class ToolsErrorResolverService extends BaseErrorResolverService {
  constructor() {
    super();
    this.codes = {
      ...this.codes
    };
  }
}
