import { Injectable } from '@angular/core';
import { FileDirectoryTypes } from '../../enums';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ToolsService {
  public getUploadFileUrl(fileDirectoryType = FileDirectoryTypes.DEFAULT): string {
    return `${ environment.apiUrl }/tools/upload-file?type=${ fileDirectoryType }`;
  }

  public getUploadedFileUrl(fileDirectoryType = FileDirectoryTypes.DEFAULT, filename: string): string {
    return `${ environment.serverUrl }/${ fileDirectoryType.toLowerCase() }/${ filename }`;
  }
}
