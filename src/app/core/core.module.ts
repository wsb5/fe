import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormDirective, ShowAuthedDirective } from './directives';
import { DatetimeFormatPipe, TimeFormatPipe, DateFormatPipe, FileUrlPipe } from './pipes';

@NgModule({
  declarations: [
    FormDirective,
    ShowAuthedDirective,
    FileUrlPipe,
    DateFormatPipe,
    DatetimeFormatPipe,
    TimeFormatPipe
  ],
  exports: [
    FormDirective,
    ShowAuthedDirective,
    FileUrlPipe,
    DatetimeFormatPipe,
    DateFormatPipe,
    TimeFormatPipe
  ],
  imports: [
    CommonModule
  ],
  providers: []
})
export class CoreModule { }
