export interface IClaims {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  token: string;
  refreshToken: string;
}
