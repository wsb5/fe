import { Allergen, Diet, Ingredient, Step, Substance, Time } from './ICookbutlerRecipes';
import { ICategory } from './ICategory';

export interface IRecipe {
  id: number;
  externalId: number;
  title: string;
  subtitle: string;
  alttitle: string;
  serving: number;
  firstYieldQuantity: number;
  secondYieldQuantity: number;
  yieldInfo: string;
  yieldInfoShort: string;
  difficulty: string;
  price: string;
  imageKey: string;
  times: Time[];
  allergens: Allergen[];
  ingredients: Ingredient[];
  steps: Step[];
  diets: Diet[];
  substances: Substance[];
  mainCategory: ICategory;
  categories: ICategory[];
  imagesKeys: { id: number; imageKey: number; recipeId: number; }[];
}
