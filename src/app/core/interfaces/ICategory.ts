export interface ICategory {
  id?: number;
  parentId?: number;
  key?: string;
  name?: string;
  imageKey?: string;
  description?: string;
  createdAt?: string;
  children?: ICategory[];
  level?: number;
  expand?: boolean;
  parent?: ICategory;
}
