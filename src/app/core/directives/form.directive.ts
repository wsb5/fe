import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Directive({
  selector: '[appForm]'
})
export class FormDirective {

  @Input() appForm: FormGroup;

  constructor(
    private readonly _elementRef: ElementRef<HTMLFormElement>,
    private readonly _renderer: Renderer2,
    private readonly _notificationService: NzNotificationService
  ) {
    this._renderer.listen(this._elementRef.nativeElement, 'submit', () => {
      this.formGroupsActions([this.appForm]);
      if (this.appForm.invalid) {
        this._notificationService.warning('Warning', 'Please fill all required fields.');
      }
    });
  }

  private formGroupsActions(formGroups: FormGroup[]): void {
    formGroups.forEach(formGroup => {
      for (const control in formGroup.controls) {
        if (formGroup.controls.hasOwnProperty(control)) {
          formGroup.controls[control].markAsDirty();
          formGroup.controls[control].updateValueAndValidity();
          if (formGroup.controls[control] instanceof FormArray) {
            const formArray = formGroup.controls[control] as FormArray;
            const controls = formArray.controls as FormGroup[];
            this.formGroupsActions(controls);
          }
        }
      }
    });
  }
}
