import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { JWTService } from '../services';

@Directive({
  selector: '[appShowAuthed]'
})
export class ShowAuthedDirective {

  @Input() set appShowAuthed(condition: boolean) {
    this._condition = condition;
  }

  private _condition: boolean;
  private _isCreated = false;

  constructor(
    private _templateRef: TemplateRef<any>,
    private _jwtService: JWTService,
    private _viewContainer: ViewContainerRef
  ) {}

  ngOnInit() {
    this._jwtService.isAuthenticated$.subscribe(isAuthenticated => {
      if (isAuthenticated && this._condition && !this._isCreated || !isAuthenticated && !this._condition && !this._isCreated) {
        this._isCreated = true;
        this._viewContainer.createEmbeddedView(this._templateRef);
      } else if (isAuthenticated && !this._condition || !isAuthenticated && this._condition) {
        this._viewContainer.clear();
        this._isCreated = false;
      }
    });
  }
}
