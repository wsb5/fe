import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { AuthService, JWTService } from '../services';
import { catchError, filter, switchMap, take } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class HttpTokenInterceptor implements HttpInterceptor {

  private _isRefreshing = false;
  private readonly _refreshTokenSubject$ = new BehaviorSubject<any>(null);

  constructor(
    private readonly _jwtService: JWTService,
    private readonly _authService: AuthService,
    private readonly _router: Router,
  ) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig: any = {
      'Accept': 'application/json'
    };

    if (!req.url.includes('login')) {
      const identity = this._jwtService.identity;

      if (identity) {
        headersConfig['Authorization'] = `Bearer ${ req.url.includes('refresh-token') ? identity.refreshToken : identity.token }`;
      }

      const request = req.clone({ setHeaders: headersConfig });

      return next.handle(request).pipe(
        catchError(error => {
          if (error.status === 401) {
            if (req.url.includes('refresh-token')) {
              this._jwtService.destroyIdentity();
              this._router.navigateByUrl('/auth/login');
              return throwError(error);
            } else {
              return this.handle401Error(request, next);
            }
          } else {
            return throwError(error);
          }
        })
      );
    } else {
      return next.handle(req);
    }
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this._isRefreshing) {
      this._isRefreshing = true;
      this._refreshTokenSubject$.next(null);

      return this._authService.refreshToken().pipe(
        switchMap(({ token }: { token: string }) => {
          this._isRefreshing = false;
          this._refreshTokenSubject$.next(token);
          this._jwtService.identity = { ...this._jwtService.identity, token };
          return next.handle(this.appendToken(request, token));
        }));

    } else {
      return this._refreshTokenSubject$.pipe(
        filter(token => token !== null),
        take(1),
        switchMap(jwt => next.handle(this.appendToken(request, jwt)))
      );
    }
  }

  private appendToken(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({
      setHeaders: {
        'Authorization': `Bearer ${ token }`
      }
    });
  }
}
