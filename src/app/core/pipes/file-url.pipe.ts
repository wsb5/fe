import { Pipe, PipeTransform } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FileDirectoryTypes } from '../enums';

@Pipe({
  name: 'fileUrl'
})
export class FileUrlPipe implements PipeTransform {

  public transform(fileKey: string, fileDirectoryType: FileDirectoryTypes): string {
    return `${ environment.serverUrl }/${ fileDirectoryType.toLowerCase() }/${ fileKey }`;
  }
}
