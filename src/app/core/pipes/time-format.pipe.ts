import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { environment } from '../../../environments/environment';

@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  public transform(date: string): string {
    return new DatePipe('de-DE').transform(date, environment.timeFormat);
  }
}
