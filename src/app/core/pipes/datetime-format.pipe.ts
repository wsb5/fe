import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { environment } from '../../../environments/environment';

@Pipe({
  name: 'datetimeFormat'
})
export class DatetimeFormatPipe implements PipeTransform {

  public transform(date: string): string {
    return new DatePipe('en-EN').transform(date, `${ environment.dateFormat }, ${ environment.timeFormat }`);
  }
}
