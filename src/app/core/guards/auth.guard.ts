import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { take, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { JWTService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(
    private readonly _router: Router,
    private readonly _jwtService: JWTService
  ) {}

  public canActivate(): Observable<boolean> {
    return this.canActiveAnything();
  }

  public canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.canActiveAnything();
  }

  private canActiveAnything(): Observable<any> {
    return this._jwtService.isAuthenticated$.pipe(
      take(1),
      tap(authState => {
        if (!authState) {
          this._router.navigateByUrl('/auth/login');
        }
      })
    );
  }
}
