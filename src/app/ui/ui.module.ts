import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchComponent } from './search/search.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { PageComponent } from './page/page.component';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { FormsModule } from '@angular/forms';
import { AvatarUploaderComponent } from './uploaders/avatar-uploader/avatar-uploader.component';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { MultiUploaderComponent } from './uploaders/multi-uploader/multi-uploader.component';
import { CoreModule } from '../core/core.module';

@NgModule({
  declarations: [
    SearchComponent,
    PageComponent,
    AvatarUploaderComponent,
    RichTextEditorComponent,
    MultiUploaderComponent
  ],
  exports: [
    SearchComponent,
    PageComponent,
    AvatarUploaderComponent,
    RichTextEditorComponent,
    MultiUploaderComponent,
  ],
  imports: [
    CommonModule,
    NzButtonModule,
    NzInputModule,
    NzIconModule,
    NzPageHeaderModule,
    FormsModule,
    NzUploadModule,
    NzNotificationModule,
    AngularEditorModule,
    CoreModule
  ]
})
export class UiModule { }
