import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponent implements OnInit {

  @Output() searchChanged = new EventEmitter<string>();

  public search: string = null;

  private readonly _searchChanged$ = new Subject<void>();

  constructor(
    private readonly _cdr: ChangeDetectorRef
  ) { }

  public ngOnInit(): void {
    this._searchChanged$.pipe(debounceTime(1000)).subscribe(() => {
      if (this.search !== null) {
        this.searchChanged.emit(this.search);
      }
    });
  }

  public searchChange(): void {
    this._searchChanged$.next();
  }

  public clearSearch(): void {
    this.search = null;
    this._cdr.detectChanges();
  }
}
