import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { FileDirectoryTypes } from '../../../core/enums';
import { ToolsErrorResolverService, ToolsService } from '../../../core/services/tools';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-avatar-uploader',
  templateUrl: './avatar-uploader.component.html',
  styleUrls: ['./avatar-uploader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarUploaderComponent implements OnInit {

  @Input() fileDirectoryType = FileDirectoryTypes.DEFAULT;
  @Input() set imageKey(imageKey: string) {
    if (imageKey) {
      this.imageUrl = this._toolsService.getUploadedFileUrl(this.fileDirectoryType, imageKey);
    }
  }

  @Output() imageKeyChanged = new EventEmitter<string>();
  @Output() loadingChanged = new EventEmitter<boolean>();

  public loading = false;
  public uploadUrl: string;
  public imageUrl: string = null;

  constructor(
    private readonly _toolsService: ToolsService,
    private readonly _toolsErrorResolverService: ToolsErrorResolverService,
    private readonly _notificationService: NzNotificationService,
  ) { }

  public ngOnInit(): void {
    this.uploadUrl = this._toolsService.getUploadFileUrl(this.fileDirectoryType);
  }

  public handleChange(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        this.loading = true;
        this.loadingChanged.emit(true);
        break;
      case 'done':
        const { file: { response: { filename } } } = info;
        this.imageKeyChanged.emit(filename);
        this.imageUrl = this._toolsService.getUploadedFileUrl(this.fileDirectoryType, filename);
        this.loading = false;
        this.loadingChanged.emit(false);
        break;
      case 'error':
        this.loading = false;
        this.imageUrl = null;
        this.imageKeyChanged.emit(null);
        this.loadingChanged.emit(false);
        this._notificationService.error('Fehler', this._toolsErrorResolverService.getMessage(info.file.error.error.internalCode))
        break;
    }
  }

  public removeImage(): void {
    this.imageUrl = null;
    this.loading = false;
    this.imageKeyChanged.emit(null);
  }
}
