import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { NzUploadFile } from 'ng-zorro-antd/upload';
import { ToolsErrorResolverService, ToolsService } from '../../../core/services/tools';
import { FileDirectoryTypes } from '../../../core/enums';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-multi-uploader',
  templateUrl: './multi-uploader.component.html',
  styleUrls: ['./multi-uploader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiUploaderComponent implements OnInit {

  @Input() accept = 'image/png, image/jpeg, image/jpg';
  @Input() fileDirectoryType = FileDirectoryTypes.DEFAULT;
  @Input() files: NzUploadFile[] = [];

  @Output() imagesKeysChanged = new EventEmitter<string[]>();
  @Output() loadingChanged = new EventEmitter<boolean>();

  public uploadUrl: string;

  constructor(
    private readonly _toolsService: ToolsService,
    private readonly _notificationService: NzNotificationService,
    private readonly _toolsErrorResolverService: ToolsErrorResolverService,
  ) { }

  public ngOnInit(): void {
    this.uploadUrl = this._toolsService.getUploadFileUrl(this.fileDirectoryType);
  }

  public handleChange(info: { file: NzUploadFile }): void {
    switch (info.file.status) {
      case 'uploading':
        this.loadingChanged.emit(true);
        break;
      case 'done':
        const { file: { response: { filename } } } = info;
        this.files.find(a => a.uid === info.file.uid).imageKey = filename;
        this.imagesChanged();
        if (this.files.every(a => a.imageKey)) {
          this.loadingChanged.emit(false);
        }
        break;
      case 'error':
        this.files = [];
        this.imagesChanged();
        this.loadingChanged.emit(false);
        this._notificationService.error('Fehler', this._toolsErrorResolverService.getMessage(info.file.error.error.internalCode))
        break;
    }
  }

  public removeImage(file: NzUploadFile): void {
    this.files = this.files.filter(a => a !== file);
    this.imagesChanged();
  }

  public get imagesKeys(): string[] {
    return this.files.filter(a => a.imageKey).map(a => a.imageKey);
  }

  private imagesChanged(): void {
    this.imagesKeysChanged.emit(this.imagesKeys);
  }
}
