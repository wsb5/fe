import { ChangeDetectorRef } from '@angular/core';

export abstract class ModalGeneric {

  public isVisible = false;
  public loading = false;

  protected constructor(
    protected readonly cdr: ChangeDetectorRef
  ) {}

  public showModal(): void {
    this.isVisible = true;
    this.cdr.detectChanges();
  }

  public closeModal(): void {
    this.isVisible = false;
    this.loading = false;
  }
}
