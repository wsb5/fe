import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';
import { JWTService } from '../../../../core/services';

@Component({
  selector: 'app-login-page',
  template: ``,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogOutPageComponent implements OnInit {

  constructor(
    private readonly _notificationService: NzNotificationService,
    private readonly _jwtService: JWTService,
    private readonly _router: Router,
  ) { }

  public ngOnInit(): void {
    this._jwtService.destroyIdentity();
    this._router.navigateByUrl('');
    this._notificationService.success('Success', 'Signed out successfully.');
  }
}
