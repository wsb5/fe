import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AuthErrorResolverService, AuthService, JWTService } from '../../../../core/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginPageComponent implements OnInit {

  public form: FormGroup;
  public loading = false;

  private readonly _formFields = {
    username: ['', Validators.required],
    password: ['', Validators.required]
  };

  constructor(
    private readonly _formBuilder: FormBuilder,
    private readonly _authService: AuthService,
    private readonly _jwtService: JWTService,
    private readonly _authErrorResolverService: AuthErrorResolverService,
    private readonly _notificationService: NzNotificationService,
    private readonly _router: Router,
    private readonly _cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.form = this._formBuilder.group(this._formFields);
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }

    this.loading = true;

    this._authService.login(this.form.value).pipe(
      finalize(() => {
        this.loading = false;
        this._cdr.detectChanges();
      })
    ).subscribe(identity => {
      this._jwtService.identity = identity;
      this._notificationService.success('Success', 'Correctly signed in. Wait.');
      this._router.navigateByUrl('');
    }, error => this._notificationService.error('Error', this._authErrorResolverService.getMessage(error.error.internalCode)));
  }
}
