import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { ReactiveFormsModule } from '@angular/forms';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { CoreModule } from '../../core/core.module';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { LogOutPageComponent } from './pages/log-out-page/log-out-page.component';


@NgModule({
  declarations: [
    LoginPageComponent,
    LogOutPageComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    NzGridModule,
    NzFormModule,
    NzInputModule,
    ReactiveFormsModule,
    NzButtonModule,
    NzLayoutModule,
    CoreModule,
    NzNotificationModule
  ]
})
export class AuthModule { }
