import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, Input } from '@angular/core';
import { AddUserModalComponent } from '../add-user-modal/add-user-modal.component';
import { FormBuilder } from '@angular/forms';
import { UsersErrorResolverService, UsersService } from '../../../../core/services';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { IUser } from '../../../../core/interfaces';
import { finalize } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-edit-user-modal',
  templateUrl: '../add-user-modal/add-user-modal.component.html',
  styleUrls: ['../add-user-modal/add-user-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditUserModalComponent extends AddUserModalComponent implements OnInit {

  @Input() set userId(obj: { userId: number; }) {
    if (obj) {
      this.loading = true;
      this.usersService.getById(obj.userId).pipe(
        finalize(() => {
          this.loading = false;
          this.cdr.detectChanges();
        })
      ).subscribe(user => {
        this.form.patchValue(user);
        this._user = user;
      }, error => {
        this.notificationService.error('Error', this.usersErrorResolverService.getMessage(error.error.internalCode));
        super.closeModal();
      });
    }
  }

  public isEdit = true;
  public title = 'Edit user';

  private _user: IUser;

  constructor(
    protected readonly cdr: ChangeDetectorRef,
    protected readonly formBuilder: FormBuilder,
    protected readonly usersService: UsersService,
    protected readonly notificationService: NzNotificationService,
    protected readonly usersErrorResolverService: UsersErrorResolverService,
    protected readonly nzModalService: NzModalService,
  ) {
    super(cdr, formBuilder, usersService, notificationService, usersErrorResolverService, nzModalService);
  }

  public ngOnInit(): void {
    super.ngOnInit();
  }

  public resetPassword(): void {
    this.loading = true;
    this.usersService.resetPassword().pipe(
      finalize(() => {
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe(({ password }) => {
      this.openModalWithPassword(password);
    }, error => this.notificationService.error('Error', this.usersErrorResolverService.getMessage(error.error.internalCode)));
  }

  protected apiCall(user: IUser): void {
    this.usersService.update(this._user.id, user).pipe(
      finalize(() => {
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe(() => {
      super.closeModal();
      this.reloadList.emit();
      this.notificationService.success('Success', 'User updated.');
    }, error => this.notificationService.error('Error', this.usersErrorResolverService.getMessage(error.error.internalCode)));
  }
}
