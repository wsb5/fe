import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ModalGeneric } from '../../../../ui/generics';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersErrorResolverService, UsersService } from '../../../../core/services';
import { IUser } from '../../../../core/interfaces';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { finalize } from 'rxjs/operators';
import { NzModalService } from 'ng-zorro-antd/modal';

@Component({
  selector: 'app-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddUserModalComponent extends ModalGeneric implements OnInit {

  @Output() reloadList = new EventEmitter<void>();

  public title = 'Add user';
  public form: FormGroup = null;
  public isEdit = false;

  private readonly _formFields = {
    'firstName': ['', Validators.required],
    'lastName': ['', Validators.required],
    'username': ['', Validators.required],
    'email': ['', Validators.compose([Validators.required, Validators.email])],
  };

  constructor(
    protected readonly cdr: ChangeDetectorRef,
    protected readonly formBuilder: FormBuilder,
    protected readonly usersService: UsersService,
    protected readonly notificationService: NzNotificationService,
    protected readonly usersErrorResolverService: UsersErrorResolverService,
    protected readonly nzModalService: NzModalService,
  ) {
    super(cdr);
  }

  public ngOnInit(): void {}

  public showModal() {
    super.showModal();
    this.form = this.formBuilder.group(this._formFields);
  }

  public submitForm(): void {
    if (this.form.valid) {
      this.loading = true;
      const user: IUser = this.form.value;
      this.apiCall(user);
    }
  }

  public afterCloseModal(): void {
    this.form = null;
  }

  public resetPassword(): void {}

  protected apiCall(user: IUser): void {
    this.usersService.create(user).pipe(
      finalize(() => {
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe(({ password }) => {
      this.notificationService.success('Success', 'User created.');
      super.closeModal();
      this.openModalWithPassword(password);
      this.reloadList.emit();
    }, error => this.notificationService.error('Error', this.usersErrorResolverService.getMessage(error.error.internalCode)));
  }

  protected openModalWithPassword(password: string): void {
    this.nzModalService.create({
      nzTitle: `Password for user: ${ this.form.get('username').value }`,
      nzClassName: 'password-modal',
      nzContent: `<h3 class="password">${ password }</h3>`,
    });
  }
}
