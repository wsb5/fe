import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IIdentity, IUser } from '../../../../core/interfaces';
import { JWTService, UsersErrorResolverService, UsersService } from '../../../../core/services';
import { finalize } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { EditUserModalComponent } from '../../modals/edit-user-modal/edit-user-modal.component';

@Component({
  selector: 'app-users-page',
  templateUrl: './users-page.component.html',
  styleUrls: ['./users-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsersPageComponent implements OnInit {

  @ViewChild('editUserModalComponent') editUserModalComponent: EditUserModalComponent;

  public users: IUser[] = [];
  public loading = false;
  public userId: { userId: number; } = null;
  public identity: IIdentity;

  private _search = '';

  constructor(
    private readonly _usersService: UsersService,
    private readonly _usersErrorResolverService: UsersErrorResolverService,
    private readonly _notificationService: NzNotificationService,
    private readonly _cdr: ChangeDetectorRef,
    private readonly _jwtService: JWTService,
  ) { }

  public ngOnInit(): void {
    this.identity = this._jwtService.identity;
    this.loadUsers();
  }

  public loadUsers(): void {
    this.loading = true;
    this._usersService.getAll(this._search).pipe(
      finalize(() => {
        this.loading = false;
        this._cdr.detectChanges();
      }),
    ).subscribe(users => this.users = users, error => {
      this._notificationService.error('Fehler', this._usersErrorResolverService.getMessage(error.error.internalCode));
    });
  }

  public deleteUser(id: number): void {
    this.loading = true;
    this._usersService.delete(id).subscribe(this.loadUsers.bind(this), error => {
      this._notificationService.error('Fehler', this._usersErrorResolverService.getMessage(error.error.internalCode));
    });
  }

  public openEditModal(userId: number): void {
    this.userId = { userId };
    this.editUserModalComponent.showModal();
  }

  public reloadList(): void {
    this.loadUsers();
  }

  public searchChanged(phrase: string): void {
    this._search = phrase;
    this.loadUsers();
  }
}
