import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersPageComponent } from './pages/users-page/users-page.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { UiModule } from '../../ui/ui.module';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { AddUserModalComponent } from './modals/add-user-modal/add-user-modal.component';
import { EditUserModalComponent } from './modals/edit-user-modal/edit-user-modal.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { CoreModule } from '../../core/core.module';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzInputModule } from 'ng-zorro-antd/input';


@NgModule({
  declarations: [
    UsersPageComponent,
    AddUserModalComponent,
    EditUserModalComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NzButtonModule,
    NzIconModule,
    NzGridModule,
    UiModule,
    NzPageHeaderModule,
    NzTableModule,
    NzPopconfirmModule,
    NzNotificationModule,
    NzModalModule,
    ReactiveFormsModule,
    NzFormModule,
    CoreModule,
    NzSpinModule,
    NzInputModule,
  ]
})
export class UsersModule { }
