import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesPageComponent } from './pages/categories-page/categories-page.component';
import { NzTableModule } from 'ng-zorro-antd/table';
import { UiModule } from '../../ui/ui.module';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { CoreModule } from '../../core/core.module';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { AddCategoryModalComponent } from './modals/add-category-modal/add-category-modal.component';
import { EditCategoryModalComponent } from './modals/edit-category-modal/edit-category-modal.component';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { ReactiveFormsModule } from '@angular/forms';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTreeSelectModule } from 'ng-zorro-antd/tree-select';
import { ResortCategoriesModalComponent } from './modals/resort-categories-modal/resort-categories-modal.component';
import { NzTreeModule } from 'ng-zorro-antd/tree';


@NgModule({
  declarations: [
    CategoriesPageComponent,
    AddCategoryModalComponent,
    EditCategoryModalComponent,
    ResortCategoriesModalComponent
  ],
  imports: [
    CommonModule,
    CategoriesRoutingModule,
    NzTableModule,
    UiModule,
    NzGridModule,
    NzIconModule,
    NzButtonModule,
    CoreModule,
    NzImageModule,
    NzPopconfirmModule,
    NzModalModule,
    NzSpinModule,
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzSelectModule,
    NzTreeSelectModule,
    NzTreeModule
  ]
})
export class CategoriesModule { }
