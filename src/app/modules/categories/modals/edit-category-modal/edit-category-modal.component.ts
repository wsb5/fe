import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { finalize } from 'rxjs/operators';
import { ICategory } from '../../../../core/interfaces';
import { AddCategoryModalComponent } from '../add-category-modal/add-category-modal.component';
import { CategoriesErrorResolverService, CategoriesService } from '../../../../core/services/categories';
import { NzTreeNodeOptions } from 'ng-zorro-antd/core/tree';

@Component({
  selector: 'app-edit-category-modal',
  templateUrl: '../add-category-modal/add-category-modal.component.html',
  styleUrls: ['../add-category-modal/add-category-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCategoryModalComponent extends AddCategoryModalComponent implements OnInit {

  @Input() categoryId: { categoryId: number; };

  public isEdit = true;
  public title = 'Edit category';

  private _category: ICategory;

  constructor(
    protected readonly cdr: ChangeDetectorRef,
    protected readonly formBuilder: FormBuilder,
    protected readonly categoriesService: CategoriesService,
    protected readonly notificationService: NzNotificationService,
    protected readonly categoriesErrorResolverService: CategoriesErrorResolverService,
  ) {
    super(cdr, formBuilder, categoriesService, notificationService, categoriesErrorResolverService);
  }

  public ngOnInit(): void {
    super.ngOnInit();
  }

  protected apiCall(category: ICategory): void {
    this.categoriesService.update(this._category.id, category).pipe(
      finalize(() => {
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe(() => {
      super.closeModal();
      this.reloadList.emit();
      this.notificationService.success('Success', 'Category updated.');
    }, error => this.notificationService.error('Error', this.categoriesErrorResolverService.getMessage(error.error.internalCode)));
  }

  protected getCategory(): void {
    if (this.categoryId) {
      this.cdr.detectChanges();
      this.categoriesService.getById(this.categoryId.categoryId).pipe(
        finalize(() => {
          this.loading = false;
          this.cdr.detectChanges();
        })
      ).subscribe(category => {
        delete category.key;
        this.form.patchValue(category);
        const fCategory = this.categories.find(a => a.id === category.parentId);
        this.form.get('key').patchValue(fCategory ? fCategory.key : 'main');
        this._category = category;
        let fCategoryTreeElement: NzTreeNodeOptions = null;

        const findCategoryTreeEl = (children = this.treeCategories.filter(a => a.id !== -1)): void => {
          children.forEach(a => a.id === this._category.id ? fCategoryTreeElement = a : findCategoryTreeEl(a.children));
        };
        findCategoryTreeEl();

        if (fCategoryTreeElement) {
          fCategoryTreeElement.disabled = true;
          fCategoryTreeElement.expanded = false;
          fCategoryTreeElement.children = [];
        }
      }, error => {
        this.notificationService.error('Error', this.categoriesErrorResolverService.getMessage(error.error.internalCode));
        super.closeModal();
      });
    }
  }
}
