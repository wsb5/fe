import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { ModalGeneric } from '../../../../ui/generics';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileDirectoryTypes } from '../../../../core/enums';
import { ICategory } from '../../../../core/interfaces';
import { finalize } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { CategoriesErrorResolverService, CategoriesService } from '../../../../core/services/categories';
import { NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd/core/tree';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-add-category-modal',
  templateUrl: './add-category-modal.component.html',
  styleUrls: ['./add-category-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddCategoryModalComponent extends ModalGeneric implements OnInit {

  @Output() reloadList = new EventEmitter<void>();

  public title = 'Add category';
  public form: FormGroup = null;
  public isEdit = false;
  public uploading = false;
  public treeCategories: NzTreeNodeOptions[] = [];
  public readonly fileDirectoryTypes = FileDirectoryTypes;

  protected categories: ICategory[];

  private readonly _formFields = {
    'name': ['', Validators.required],
    'imageKey': [''],
    'description': [''],
    'key': [null, Validators.required]
  };

  constructor(
    protected readonly cdr: ChangeDetectorRef,
    protected readonly formBuilder: FormBuilder,
    protected readonly categoriesService: CategoriesService,
    protected readonly notificationService: NzNotificationService,
    protected readonly categoriesErrorResolverService: CategoriesErrorResolverService,
  ) {
    super(cdr);
  }

  public ngOnInit(): void {

  }

  public showModal() {
    super.showModal();
    this.loadCategories();
    this.form = this.formBuilder.group(this._formFields);
  }

  public submitForm(): void {
    if (this.form.valid) {
      this.loading = true;
      const category: ICategory = this.form.value;
      const parent = this.categories.find(a => a.key === category.key);

      if (category.key === 'main') {
        const mainCategories = this.categories.filter(a => a.parentId === null).sortByKey('key');
        console.log(mainCategories);
        category.key = mainCategories.length > 0 ? (parseInt(mainCategories[mainCategories.length - 1].key, 10) + 1).toString() : '1';
        category.parentId = null;
      } else {
        if (parent.children.length > 0) {
          const lastChild = parent.children[parent.children.length - 1];
          const fragments = lastChild.key.split('-');
          fragments[fragments.length - 1] = (parseInt(fragments[fragments.length - 1], 10) + 1).toString();
          category.key = fragments.join('-');
        } else {
          category.key = parent.key + '-' + '1';
        }
        category.parentId = parent.id;
      }

      this.apiCall(category);
    }
  }

  public afterCloseModal(): void {
    this.form = null;
  }

  public nzDisplayWith(node: NzTreeNode): string {
    return node.origin.name;
  }

  protected apiCall(category: ICategory): void {
    this.categoriesService.create(category).pipe(
      finalize(() => {
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe(() => {
      this.notificationService.success('Success', 'Category created.');
      super.closeModal();
      this.reloadList.emit();
    }, error => this.notificationService.error('Error', this.categoriesErrorResolverService.getMessage(error.error.internalCode)));
  }

  protected getCategory(): void {
    this.loading = false;
    this.cdr.detectChanges();
  }

  private loadCategories(): void {
    this.loading = true;
    forkJoin(this.categoriesService.getAllAsTree(), this.categoriesService.getAll()).subscribe(([treeCategories, allCategories]) => {
      this.categories = allCategories;
      this.treeCategories = [
        {
          id: -1,
          name: 'Main category',
          expand: true,
          key: 'main',
        },
        ...treeCategories.map(a => {
          return {
            ...a,
            expand: true,
            children: a.children
          } as any;
        })
      ];

      this.getCategory();
    }, error => {
      this.loading = false;
      this.cdr.detectChanges();
      this.notificationService.error('Error', this.categoriesErrorResolverService.getMessage(error.error.internalCode));
    });
  }
}
