import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, ChangeDetectorRef, ViewChild } from '@angular/core';
import { ModalGeneric } from '../../../../ui/generics';
import { ICategory } from '../../../../core/interfaces';
import { CategoriesErrorResolverService, CategoriesService } from '../../../../core/services/categories';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NzFormatEmitEvent, NzTreeComponent } from 'ng-zorro-antd/tree';
import { NzTreeNode, NzTreeNodeOptions } from 'ng-zorro-antd/core/tree';
import { forkJoin, Observable } from 'rxjs';
import * as cloneDeep from 'lodash/cloneDeep'
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-resort-categories-modal',
  templateUrl: './resort-categories-modal.component.html',
  styleUrls: ['./resort-categories-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResortCategoriesModalComponent extends ModalGeneric implements OnInit {

  @ViewChild('treeComponent') treeComponent: NzTreeComponent;

  @Output() reloadList = new EventEmitter<void>();

  public title = 'Resort categories';
  public uploading = false;
  public treeCategories: NzTreeNodeOptions[] = [];

  protected _categories: ICategory[];

  constructor(
    protected readonly cdr: ChangeDetectorRef,
    private readonly _categoriesService: CategoriesService,
    private readonly _notificationService: NzNotificationService,
    private readonly _categoriesErrorResolverService: CategoriesErrorResolverService,
  ) {
    super(cdr);
  }

  public ngOnInit(): void {}

  public showModal() {
    super.showModal();
    this.loadCategories();
  }

  public onDrop(evt: NzFormatEmitEvent): void {
    this.loading = true;
    const categories: ICategory[] = [];
    const treeNodeClone: NzTreeNode[] = cloneDeep(this.treeComponent.getTreeNodes());
    const originActions = (treeNode: NzTreeNode, key: string, parentId: number): void => {
      treeNode.origin = {
        ...treeNode.origin,
        key,
        parentId,
      };
      categories.push(treeNode.origin);
      parseTreeNodes(treeNode.children, treeNode.origin.id, treeNode.key);
    };
    const parseTreeNodes = (children: NzTreeNode[], originId: number, originKey: string): void => {
      children.forEach((a, aIndex) => originActions(a, `${ originKey }-${ aIndex + 1 }`, originId));
    };

    treeNodeClone.forEach((a, aIndex) => originActions(a, (aIndex + 1).toString(), null));

    forkJoin(...categories.map(a => {
      return this._categoriesService.update(a.id, {
        id: a.id,
        parentId: a.parentId,
        key: a.key,
        name: a.name,
        imageKey: a.imageKey,
        description: a.description,
      });
    })).pipe(
      finalize(() => {
        this.loading = false;
        this.cdr.detectChanges();
      })
    ).subscribe(() => {
      this.reloadList.emit();
    }, error => this._notificationService.error('Error', this._categoriesErrorResolverService.getMessage(error.error.internalCode)));
  }

  private loadCategories(): void {
    this.loading = true;
    forkJoin(
      this._categoriesService.getAllAsTree(),
      this._categoriesService.getAll()
    ).subscribe(([treeCategories, allCategories]) => {
      this._categories = allCategories;
      this.treeCategories = [
        ...treeCategories.map(a => {
          return {
            ...a,
            expanded: true,
            children: a.children
          } as any;
        })
      ];
      this.parseTreeCategories(this.treeCategories);
      this.loading = false;
      this.cdr.detectChanges();
    }, error => {
      this.loading = false;
      this.cdr.detectChanges();
      this._notificationService.error('Error', this._categoriesErrorResolverService.getMessage(error.error.internalCode));
    });
  }

  private parseTreeCategories(tree: NzTreeNodeOptions[]): void {
    tree.forEach(a => {
      a.title = a.name;
      a.expanded = true;
      this.parseTreeCategories(a.children);
    });
  }
}
