import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FileDirectoryTypes } from '../../../../core/enums';
import { ICategory } from '../../../../core/interfaces';
import { finalize } from 'rxjs/operators';
import { CategoriesErrorResolverService, CategoriesService } from '../../../../core/services/categories';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { EditCategoryModalComponent } from '../../modals/edit-category-modal/edit-category-modal.component';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesPageComponent implements OnInit {

  @ViewChild('editCategoryModalComponent') editCategoryModalComponent: EditCategoryModalComponent;

  public loading = false;
  public categories: ICategory[] = [];
  public mapOfExpandedData: { [key: string]: ICategory[] } = {};
  public categoryId: { categoryId: number; } = null;
  public areAllExpanded = false;
  public readonly fileDirectoryTypes = FileDirectoryTypes;

  private _search = '';

  constructor(
    private readonly _categoriesService: CategoriesService,
    private readonly _categoriesErrorResolverService: CategoriesErrorResolverService,
    private readonly _notificationService: NzNotificationService,
    private readonly _cdr: ChangeDetectorRef,
  ) { }

  public ngOnInit(): void {
    this.loadCategories();
  }

  public reloadList(): void {
    this.loadCategories();
  }

  public collapse(array: ICategory[], data: ICategory, $event: boolean): void {
    if (!$event) {
      if (data.children) {
        data.children.forEach(d => {
          const target = array.find(a => a.key === d.key)!;
          target.expand = false;
          this.collapse(array, target, false);
        });
      } else {
        return;
      }
    }
  }

  public searchChanged(phrase: string): void {
    this._search = phrase;
    this.loadCategories();
  }

  public deleteCategory(id: number): void {
    this.loading = true;
    this._categoriesService.delete(id).subscribe(this.loadCategories.bind(this), error => {
      this._notificationService.error('Fehler', this._categoriesErrorResolverService.getMessage(error.error.internalCode));
    });
  }

  public openEditModal(id: number): void {
    this.categoryId = { categoryId: id };
    this.editCategoryModalComponent.showModal();
  }

  public toggleExpand(): void {
    this.areAllExpanded = !this.areAllExpanded;

    const markAsExpanded = (categories: ICategory[]): void => {
      categories.forEach(a => {
        a.expand = this.areAllExpanded;
        markAsExpanded(a.children);
      });
    };

    for (const key in this.mapOfExpandedData) {
      markAsExpanded(this.mapOfExpandedData[key]);
    }
  }

  private loadCategories(): void {
    this.loading = true;
    this.mapOfExpandedData = {};
    this._categoriesService.getAllAsTree(this._search).pipe(
      finalize(() => {
        this.loading = false;
        this._cdr.detectChanges();
      }),
    ).subscribe(categories => {
      this.categories = categories;
      this.categories.forEach(item => this.mapOfExpandedData[item.key] = this.convertTreeToList(item));
    }, error => {
      this._notificationService.error('Fehler', this._categoriesErrorResolverService.getMessage(error.error.internalCode));
    });
  }

  private convertTreeToList(root: ICategory): ICategory[] {
    const stack: ICategory[] = [];
    const array: ICategory[] = [];
    const hashMap = {};
    stack.push({ ...root, level: 0, expand: false });

    while (stack.length !== 0) {
      const node = stack.pop()!;
      this.visitNode(node, hashMap, array);
      if (node.children) {
        for (let i = node.children.length - 1; i >= 0; i--) {
          stack.push({ ...node.children[i], level: node.level! + 1, expand: false, parent: node });
        }
      }
    }

    return array;
  }

  private visitNode(node: ICategory, hashMap: { [key: string]: boolean }, array: ICategory[]): void {
    if (!hashMap[node.key]) {
      hashMap[node.key] = true;
      array.push(node);
    }
  }
}
