export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000/api/v1',
  serverUrl: 'http://localhost:3000',
  dateFormat: 'dd.MM.yyyy',
  timeFormat: 'HH:mm',
  defaultPaginationResults: 12,
};
